package twpathashala51.palash.gameoflife;

import java.util.HashSet;
import java.util.Set;

// Models the state of the gameboard
public class GameBoard {
  private final Set<Cell> aliveCells;
  private final Set<Cell> deadCells;

  GameBoard(Set<Cell> seedCoordinates) {
    this.aliveCells = seedCoordinates;
    deadCells = generateDeadNeighboursOfLiveCells();
  }

  private Set<Cell> aliveCellsThatStayAlive() {
    Set<Cell> aliveCellsThatAreAliveInNextGeneration = new HashSet<>();
    for (Cell currentCell : aliveCells) {
      if (currentCell.staysForNextGeneration(aliveCells))
        aliveCellsThatAreAliveInNextGeneration.add(currentCell);
    }
    return aliveCellsThatAreAliveInNextGeneration;
  }

  private Set<Cell> deadCellsThatBecomeAlive() {
    Set<Cell> deadCellsThatBecomeAliveInNextGeneration = new HashSet<>();
    for (Cell currentCell : deadCells) {
      currentCell.generateNeighbours();
      if (currentCell.canComeBackToLife(aliveCells))
        deadCellsThatBecomeAliveInNextGeneration.add(currentCell);
    }
    return deadCellsThatBecomeAliveInNextGeneration;
  }

  private Set<Cell> generateDeadNeighboursOfLiveCells() {
    Set<Cell> deadCells = new HashSet<>();
    Set<Cell> deadNeighbours;
    for (Cell currentCell : aliveCells) {
      currentCell.generateNeighbours();
      deadNeighbours = currentCell.getDeadNeighbours(aliveCells);
      deadCells.addAll(deadNeighbours);
    }
    return deadCells;
  }

  GameBoard playNextRound() {
    Set<Cell> nextGeneration = new HashSet<>();
    nextGeneration.addAll(aliveCellsThatStayAlive());
    nextGeneration.addAll(deadCellsThatBecomeAlive());
    return new GameBoard(nextGeneration);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    GameBoard gameBoard = (GameBoard) o;

    if (aliveCells != null ? !aliveCells.equals(gameBoard.aliveCells) : gameBoard.aliveCells != null) return false;
    return deadCells != null ? deadCells.equals(gameBoard.deadCells) : gameBoard.deadCells == null;

  }

  @Override
  public int hashCode() {
    int result = aliveCells != null ? aliveCells.hashCode() : 0;
    result = 31 * result + (deadCells != null ? deadCells.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "GameBoard{" +
        "aliveCells=" + aliveCells +
        '}';
  }
}
