package twpathashala51.palash.gameoflife;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class GameOfLifeDriver {

  private static final String INPUT_FILENAME = "res/twpathashala51/palash/gameoflife/input.txt";
  private static final String OUTPUT_FILENAME = "res/twpathashala51/palash/gameoflife/output.txt";

  private Set<Cell> readInput(Reader readerInterface) {
    Set<Cell> seedCoordinates = new HashSet<>();
    try {
      BufferedReader inputBuffer = new BufferedReader(readerInterface);
      String line = null;
      while (((line = inputBuffer.readLine()) != null) && (!line.equals(""))) {
        seedCoordinates.add(parseLine(line));
      }
      inputBuffer.close();
    } catch (FileNotFoundException e) {
      System.out.println("The file was not found. Exiting");
      System.exit(1);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return seedCoordinates;
  }

  private Cell parseLine(String line) {
    int xCoordinate = Integer.parseInt(line.split(",")[0]);
    int yCoordinate = Integer.parseInt(line.split(",")[1].replace(" ", ""));
    return new Cell(xCoordinate, yCoordinate);
  }

  private void printOutput(Writer outputInterface, GameBoard nextGeneration) {
    try {
      BufferedWriter outputWriter = new BufferedWriter(outputInterface);
      outputWriter.write(nextGeneration.toString());
      outputWriter.newLine();
      outputWriter.close();
    } catch (IOException e) {
      System.out.println("There was an error in trying to print the output.");
    }
  }

  public static void main(String args[]) {
    GameOfLifeDriver gameDriver = new GameOfLifeDriver();
    Reader readerInterface = new InputStreamReader(System.in);
    Set<Cell> seedCoordinates = gameDriver.readInput(readerInterface);
    GameBoard newGame = new GameBoard(seedCoordinates);
    GameBoard nextGeneration = newGame.playNextRound();
    Writer outputInterface = new OutputStreamWriter(System.out);
    gameDriver.printOutput(outputInterface, nextGeneration);

  }
}
