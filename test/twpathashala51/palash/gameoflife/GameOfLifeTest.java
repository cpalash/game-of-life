package twpathashala51.palash.gameoflife;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class GameOfLifeTest {
  private Cell liveCell;
  private Set<Cell> seedDimensions;
  private GameBoard presentGeneration;
  private Set<Cell> expectedNextGeneration;
  private GameBoard nextGeneration;


  @Before
  public void setUp() throws Exception {
    seedDimensions = new HashSet<>();
    expectedNextGeneration = new HashSet<>();
  }

  @Test
  public void aCellWithNoNeighboursShouldDieInNextTick() {
    liveCell = new Cell(0, 0);
    seedDimensions.add(liveCell);
    presentGeneration = new GameBoard(seedDimensions);
    nextGeneration = presentGeneration.playNextRound();
    Assert.assertEquals(nextGeneration, new GameBoard(expectedNextGeneration));
  }

  @Test
  public void aDeadCellWithThreeNeighboursShouldReincarnate() {
    liveCell = new Cell(0, 0);
    Cell secondCell = new Cell(0, 1);
    Cell thirdCell = new Cell(1, 0);
    seedDimensions.add(liveCell);
    seedDimensions.add(secondCell);
    seedDimensions.add(thirdCell);

    presentGeneration = new GameBoard(seedDimensions);

    nextGeneration = presentGeneration.playNextRound();

    expectedNextGeneration.add(new Cell(0, 0));
    expectedNextGeneration.add(new Cell(0, 1));
    expectedNextGeneration.add(new Cell(1, 0));
    expectedNextGeneration.add(new Cell(1, 1));

    Assert.assertEquals(nextGeneration, new GameBoard(expectedNextGeneration));
  }

  @Test
  public void aBlockPatternShouldHaveStillLife() {
    liveCell = new Cell(1, 1);
    Cell secondLiveCell = new Cell(1, 2);
    Cell thirdLiveCell = new Cell(2, 1);
    Cell fourthLiveCell = new Cell(2, 2);

    seedDimensions.add(liveCell);
    seedDimensions.add(secondLiveCell);
    seedDimensions.add(thirdLiveCell);
    seedDimensions.add(fourthLiveCell);

    presentGeneration = new GameBoard(seedDimensions);

    Cell firstExpectedLiveCell = new Cell(1, 1);
    Cell secondExpectedLiveCell = new Cell(1, 2);
    Cell thirdExpectedLiveCell = new Cell(2, 1);
    Cell fourthExpectedLiveCell = new Cell(2, 2);

    expectedNextGeneration.add(firstExpectedLiveCell);
    expectedNextGeneration.add(secondExpectedLiveCell);
    expectedNextGeneration.add(thirdExpectedLiveCell);
    expectedNextGeneration.add(fourthExpectedLiveCell);

    nextGeneration = presentGeneration.playNextRound();
    Assert.assertEquals(nextGeneration, new GameBoard(expectedNextGeneration));
  }

  @Test
  public void aBoatPatternShouldHaveStillLife() {
    liveCell = new Cell(0, 1);
    Cell secondLiveCell = new Cell(1, 0);
    Cell thirdLiveCell = new Cell(2, 1);
    Cell fourthLiveCell = new Cell(0, 2);
    Cell fifthLiveCell = new Cell(1, 2);

    seedDimensions.add(liveCell);
    seedDimensions.add(secondLiveCell);
    seedDimensions.add(thirdLiveCell);
    seedDimensions.add(fourthLiveCell);
    seedDimensions.add(fifthLiveCell);

    presentGeneration = new GameBoard(seedDimensions);

    Cell firstExpectedLiveCell = new Cell(0, 1);
    Cell secondExpectedLiveCell = new Cell(1, 0);
    Cell thirdExpectedLiveCell = new Cell(2, 1);
    Cell fourthExpectedLiveCell = new Cell(0, 2);
    Cell fifthExpectedLiveCell = new Cell(1, 2);

    expectedNextGeneration.add(firstExpectedLiveCell);
    expectedNextGeneration.add(secondExpectedLiveCell);
    expectedNextGeneration.add(thirdExpectedLiveCell);
    expectedNextGeneration.add(fourthExpectedLiveCell);
    expectedNextGeneration.add(fifthExpectedLiveCell);

    nextGeneration = presentGeneration.playNextRound();
    Assert.assertEquals(nextGeneration, new GameBoard(expectedNextGeneration));
  }

  @Test
  public void aBlinkerPatternWouldOscillateBetweenTwoStates() {
    liveCell = new Cell(1, 1);
    Cell secondLiveCell = new Cell(1, 0);
    Cell thirdLiveCell = new Cell(1, 2);

    seedDimensions.add(liveCell);
    seedDimensions.add(secondLiveCell);
    seedDimensions.add(thirdLiveCell);

    presentGeneration = new GameBoard(seedDimensions);

    Cell firstExpectedLiveCell = new Cell(1, 1);
    Cell secondExpectedLiveCell = new Cell(0, 1);
    Cell thirdExpectedLiveCell = new Cell(2, 1);

    expectedNextGeneration.add(firstExpectedLiveCell);
    expectedNextGeneration.add(secondExpectedLiveCell);
    expectedNextGeneration.add(thirdExpectedLiveCell);

    nextGeneration = presentGeneration.playNextRound();
    Assert.assertEquals(nextGeneration, new GameBoard(expectedNextGeneration));
  }

  @Test
  public void aToadPatternWouldOscillateBetweenThreeStates() {
    liveCell = new Cell(1, 1);
    Cell secondLiveCell = new Cell(1, 2);
    Cell thirdLiveCell = new Cell(1, 3);
    Cell fourthLiveCell = new Cell(2, 2);
    Cell fifthLiveCell = new Cell(2, 3);
    Cell sixthLiveCell = new Cell(2, 4);

    seedDimensions.add(liveCell);
    seedDimensions.add(secondLiveCell);
    seedDimensions.add(thirdLiveCell);
    seedDimensions.add(fourthLiveCell);
    seedDimensions.add(fifthLiveCell);
    seedDimensions.add(sixthLiveCell);

    presentGeneration = new GameBoard(seedDimensions);

    Cell firstExpectedLiveCell = new Cell(0, 2);
    Cell secondExpectedLiveCell = new Cell(1, 1);
    Cell thirdExpectedLiveCell = new Cell(1, 4);
    Cell fourthExpectedLiveCell = new Cell(2, 1);
    Cell fifthExpectedLiveCell = new Cell(2, 4);
    Cell sixthExpectedLiveCell = new Cell(3, 3);

    expectedNextGeneration.add(firstExpectedLiveCell);
    expectedNextGeneration.add(secondExpectedLiveCell);
    expectedNextGeneration.add(thirdExpectedLiveCell);
    expectedNextGeneration.add(fourthExpectedLiveCell);
    expectedNextGeneration.add(fifthExpectedLiveCell);
    expectedNextGeneration.add(sixthExpectedLiveCell);

    nextGeneration = presentGeneration.playNextRound();
    Assert.assertEquals(nextGeneration, new GameBoard(expectedNextGeneration));
  }
}
