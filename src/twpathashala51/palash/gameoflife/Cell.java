package twpathashala51.palash.gameoflife;

import java.util.HashSet;
import java.util.Set;

// Models the smallest entity of a gameboard.
class Cell {
  private final int xCoordinate;
  private final int yCoordinate;
  private final Set<Cell> neighbours;
  private static int OVERCROWDING_CRITERIA = 3;
  private static int LONELINESS_CRITERIA = 2;
  private static int REINCARNATION_CRITERIA = 3;

  Cell(int xCoordinate, int yCoordinate) {
    this.xCoordinate = xCoordinate;
    this.yCoordinate = yCoordinate;
    this.neighbours = new HashSet<>();
  }

  void generateNeighbours() {
    for (int x = xCoordinate - 1; x <= xCoordinate + 1; x++) {
      for (int y = yCoordinate - 1; y <= yCoordinate + 1; y++) {
        if (x == xCoordinate && y == yCoordinate)
          continue;
        else
          neighbours.add(new Cell(x, y));
      }
    }
  }

  private int numberOfAliveNeighbours(Set<Cell> aliveCells) {
    int numberOfAliveNeighbours = 0;
    for (Cell neighbour : neighbours) {
      if (aliveCells.contains(neighbour))
        numberOfAliveNeighbours++;
    }
    return numberOfAliveNeighbours;
  }


  Set<Cell> getDeadNeighbours(Set<Cell> aliveCells) {
    Set<Cell> deadNeighbours = new HashSet<>();
    for (Cell neighbour : neighbours) {
      if (!aliveCells.contains(neighbour))
        deadNeighbours.add(neighbour);
    }
    return deadNeighbours;
  }

  boolean canComeBackToLife(Set<Cell> aliveCells) {
    int numberOfAliveNeighbours = numberOfAliveNeighbours(aliveCells);
    return numberOfAliveNeighbours == REINCARNATION_CRITERIA;
  }

  boolean staysForNextGeneration(Set<Cell> aliveCells) {
    int numberOfAliveNeighbours = numberOfAliveNeighbours(aliveCells);
    return numberOfAliveNeighbours == LONELINESS_CRITERIA || numberOfAliveNeighbours == OVERCROWDING_CRITERIA;
  }


  @Override
  public int hashCode() {
    return 10 * this.xCoordinate + 20 * this.yCoordinate;
  }

  @Override
  public boolean equals(Object anotherCell) {
    if (anotherCell == null)
      return false;
    if (anotherCell.getClass() != Cell.class)
      return false;
    return this.xCoordinate == ((Cell) anotherCell).xCoordinate && this.yCoordinate == ((Cell) anotherCell).yCoordinate;
  }

  @Override
  public String toString() {
    return xCoordinate + "," + yCoordinate;
  }
}
