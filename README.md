**GAME OF LIFE**

**Running Instructions :** 

_Using File IO:_

In *GameOfLifeDriver*, modify `Reader readerInterface = new InputStreamReader(System.in);` to `Reader readerInterface = new FileReader(INPUT_FILENAME);` and `Writer outputInterface = new OutputStreamWriter(System.out);` to `Reader readerInterface = new OutputWriter(OUTPUT_FILENAME);`.

In *res* folder, there is a file called *input.txt*, which is used to feed input to the program. The output is received in the file called *output.txt* in the same folder.

_Using Console IO:_

By default, the program uses console IO. An empty line indicates the end of input to the program.


